# Описание создания ВМ для мониторинга

# Данные CPU и RAM для ВМ мониторинга
data "vkcs_compute_flavor" "monitoring" {
  name = var.monitoring_flavor
}

# Данные об образе для ВМ мониторинга
data "vkcs_images_image" "monitoring" {
  name = var.monitoring_image
}

# Создание вычислительного ресурса для мониторинга
resource "vkcs_compute_instance" "monitoring" {
  name                    = "monitoring"
  flavor_id               = data.vkcs_compute_flavor.monitoring.id
  key_pair                = var.key_pair_name
  security_groups         = [vkcs_networking_secgroup.secgroup.name]
  availability_zone       = var.availability_zone_name

  block_device {
    uuid                  = data.vkcs_images_image.monitoring.id
    source_type           = "image"
    destination_type      = "volume"
    volume_type           = "ceph-ssd"
    volume_size           = 20
    boot_index            = 0
    delete_on_termination = true
  }

  network {
    uuid = vkcs_networking_network.network.id
  }

  depends_on = [
    vkcs_networking_network.network,
    vkcs_networking_subnet.subnet
  ]
}

# Вывод IP-адреса ВМ для мониторинга
output "monitoring_ip" {
  value =     [vkcs_compute_instance.monitoring.network[0].fixed_ip_v4]
}