# Описание создания балансировщика нагрузки

# Создание балансировщика нагрузки
resource "vkcs_lb_loadbalancer" "loadbalancer" {
  name = "loadbalancer"
  vip_subnet_id = vkcs_networking_subnet.subnet.id
}

# Создание прослушивателя
resource "vkcs_lb_listener" "listener" {
  name = "listener"
  protocol = "HTTP"
  protocol_port = 80
  loadbalancer_id = vkcs_lb_loadbalancer.loadbalancer.id
}

# Создание пула и типа балансировки
resource "vkcs_lb_pool" "pool" {
  name = "pool"
  protocol = "HTTP"
  lb_method = "ROUND_ROBIN"
  listener_id = vkcs_lb_listener.listener.id
}

# Создание участника пула 1
resource "vkcs_lb_member" "member_1" {
  address = vkcs_compute_instance.wp_1.network[0].fixed_ip_v4
  protocol_port = 80
  pool_id = vkcs_lb_pool.pool.id
  subnet_id = vkcs_networking_subnet.subnet.id
  weight = 5
}

# Создание участника пула 2
resource "vkcs_lb_member" "member_2" {
  address = vkcs_compute_instance.wp_2.network[0].fixed_ip_v4
  protocol_port = 80
  pool_id = vkcs_lb_pool.pool.id
  subnet_id = vkcs_networking_subnet.subnet.id
  weight = 1
}
