# Создаем кластер баз данных Galera MySQL с тремя узлами,
# Также создаем базу данных и пользователя,
# настраиваем на автоматическое резервное копирование.

# Данные о выбранном для ВМ кластера CPU и RAM.
data "vkcs_compute_flavor" "wp_db" {
  name = var.db_instance_flavor
}
# Создание кластера баз данных Galera MySQL.
# В передаётся CPU и RAM, определенное в переменной var.db_instance_flavor.
# В качестве размера хранилища указано 10 Гб, а типом хранилища является Ceph SSD.
# В качестве сети используется сеть, определенная в ресурсе vkcs_networking_network.network.
# В качестве ключевой пары для доступа к узлам кластера используется ключевая пара, определенная в переменной var.key_pair_name.
# В качестве размера кластера указано 3 узла.
# Включена автоматическая подстройка размера диска до 1000 Гб.
resource "vkcs_db_cluster" "wp_db_cluster" {
  name        = "wp-db-instance"
  availability_zone       = var.availability_zone_name
  datastore {
    type    = "galera_mysql"
    version = "8.0"
  }
  flavor_id   = data.vkcs_compute_flavor.wp_db.id
  keypair     = var.key_pair_name
  volume_size = 10
  volume_type = "ceph-ssd"
  disk_autoexpand {
    autoexpand    = true
    max_disk_size = 1000
  }
  cluster_size = 3

  network {
    uuid = vkcs_networking_network.network.id
  }

  depends_on = [
    vkcs_networking_router_interface.router_interface_lb
  ]

# Включаем возможность экспорта метрик в формате Prometheus с ВМ.
  capabilities {
    name = "node_exporter"
    settings = {
      "listen_port":"9100"
    }
  }
# Включаем возможность экспорта метрик в формате Prometheus с БД.
  capabilities {
    name = "mysqld_exporter"
    settings = {
      "listen_port":"9110"
    }
  }
}

# Создание базы данных для WordPress.
resource "vkcs_db_database" "wp_db_database" {
  name        = "wordpressdb"
  dbms_id = vkcs_db_cluster.wp_db_cluster.id
  charset     = "utf8"
}

# Создание пользователя для базы данных WordPress.
resource "vkcs_db_user" "wp_db_user" {
  name        = var.WORDPRESS_DB_USER
  password    = var.WORDPRESS_DB_PASSWORD
  dbms_id = vkcs_db_cluster.wp_db_cluster.id
  databases   = [vkcs_db_database.wp_db_database.name]
}

# Создание плана резервного копирования для базы данных.
# Резервные копии будут создаваться каждые 12 часов, а максимальное количество полных резервных копий - 3.
resource "vkcs_backup_plan" "backup_plan" {
  name          = "backup_wp_db_database"
  provider_name = "dbaas"
  incremental_backup = false
  schedule = {
    every_hours = 12
  }
  full_retention = {
    max_full_backup = 6
  }
  instance_ids = [vkcs_db_cluster.wp_db_cluster.id]
}

# Получение IP-адреса балансировщика нагрузки для кластера баз данных.
data "vkcs_lb_loadbalancer" "db_loadbalancer" {
  id = "${vkcs_db_cluster.wp_db_cluster.loadbalancer_id}"
}

data "vkcs_networking_port" "db_loadbalancer_port" {
  port_id = "${data.vkcs_lb_loadbalancer.db_loadbalancer.vip_port_id}"
}

# Вывод IP-адреса балансировщика нагрузки для кластера баз данных.
output "cluster_lb_ip" {
  value = data.vkcs_networking_port.db_loadbalancer_port.all_fixed_ips
}
    
