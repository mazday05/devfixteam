resource "vkcs_networking_secgroup" "secgroup" {
   name = "security_group"
   description = "Группа безопасности для структуры"
}

resource "vkcs_networking_secgroup_rule" "ssh" {
   direction = "ingress"
   port_range_max = 22
   port_range_min = 22
   protocol = "tcp"
   remote_ip_prefix = "0.0.0.0/0"
   security_group_id = vkcs_networking_secgroup.secgroup.id
   description = "Открываем порт ssh"
}

resource "vkcs_networking_secgroup_rule" "http_udp" {
   direction = "ingress"
   port_range_max = 80
   port_range_min = 80
   protocol = "udp"
   remote_ip_prefix = "0.0.0.0/0"
   security_group_id = vkcs_networking_secgroup.secgroup.id
   description = "Открываем порт http протокол udp"
}

resource "vkcs_networking_secgroup_rule" "http_tcp" {
   direction = "ingress"
   port_range_max = 80
   port_range_min = 80
   protocol = "tcp"
   remote_ip_prefix = "0.0.0.0/0"
   security_group_id = vkcs_networking_secgroup.secgroup.id
   description = "Открываем порт http протокол tcp"
}

resource "vkcs_networking_secgroup_rule" "mon" {
   direction = "ingress"
   port_range_max = 3000
   port_range_min = 3000
   protocol = "tcp"
   remote_ip_prefix = "0.0.0.0/0"
   security_group_id = vkcs_networking_secgroup.secgroup.id
   description = "Открываем для моиниторинга"
}

resource "vkcs_networking_secgroup_rule" "node_exporter" {
   direction = "ingress"
   port_range_max = 9100
   port_range_min = 9100
   protocol = "tcp"
   remote_ip_prefix = "10.10.10.0/24"
   security_group_id = vkcs_networking_secgroup.secgroup.id
   description = "Открываем для node_exporter"
}

resource "vkcs_networking_secgroup_rule" "node_exporter_cluster" {
   direction = "ingress"
   port_range_max = 9110
   port_range_min = 9110
   protocol = "tcp"
   remote_ip_prefix = "10.10.10.0/24"
   security_group_id = vkcs_networking_secgroup.secgroup.id
   description = "Открываем для node_exporter кластера"
}

resource "vkcs_networking_secgroup_rule" "cadvisor" {
   direction = "ingress"
   port_range_max = 8081
   port_range_min = 8081
   protocol = "tcp"
   remote_ip_prefix = "10.10.10.0/24"
   security_group_id = vkcs_networking_secgroup.secgroup.id
   description = "Открываем для cadvisor"
}
