# Описание конфигурации для создания двух ВМ для Wordpress

# Данные CPU и RAM для ВМ Wordpress
data "vkcs_compute_flavor" "wordpress" {
  name = var.wordpress_flavor
}

# Данные об образе для ВМ мониторинга
data "vkcs_images_image" "wordpress" {
  name = var.wordpress_image
}

# Создание первой ВМ Wordpress
resource "vkcs_compute_instance" "wp_1" {
  name                    = "wordpress-1"
  flavor_id               = data.vkcs_compute_flavor.wordpress.id
  key_pair                = var.key_pair_name
  security_groups         = [vkcs_networking_secgroup.secgroup.name]
  availability_zone       = var.availability_zone_name

  block_device {
    uuid                  = data.vkcs_images_image.wordpress.id
    source_type           = "image"
    destination_type      = "volume"
    volume_type           = "ceph-ssd"
    volume_size           = 20
    boot_index            = 0
    delete_on_termination = true
  }

  network {
    uuid = vkcs_networking_network.network.id
  }

  depends_on = [
    vkcs_networking_network.network,
    vkcs_networking_subnet.subnet
  ]
}

# Создание второй ВМ Wordpress
resource "vkcs_compute_instance" "wp_2" {
  name                    = "wordpress-2"
  flavor_id               = data.vkcs_compute_flavor.wordpress.id
  key_pair                = var.key_pair_name
  security_groups         = [vkcs_networking_secgroup.secgroup.name]
  availability_zone       = var.availability_zone_name

  block_device {
    uuid                  = data.vkcs_images_image.wordpress.id
    source_type           = "image"
    destination_type      = "volume"
    volume_type           = "ceph-ssd"
    volume_size           = 20
    boot_index            = 0
    delete_on_termination = true
  }

  network {
    uuid = vkcs_networking_network.network.id
  }

  depends_on = [
    vkcs_networking_network.network,
    vkcs_networking_subnet.subnet
  ]
}

# Вывод IP-адресов созданных ВМ
output "instance_ips" {
  value = concat(
    [vkcs_compute_instance.wp_1.network[0].fixed_ip_v4],
    [vkcs_compute_instance.wp_2.network[0].fixed_ip_v4]
  )
}
