# Описание создания сетевой структуры

# Получение сети external network
data "vkcs_networking_network" "extnet" {
   name = "ext-net"
}

# Создание сети-int-net
resource "vkcs_networking_network" "network" {
   name = "manage-int-net"
}

# Создание подсети subnet в сети manage-int-net
resource "vkcs_networking_subnet" "subnet" {
   name       = "subnet"
   network_id = vkcs_networking_network.network.id
   cidr       = "10.10.10.0/24"
}

# Создание маршрутизатора router
resource "vkcs_networking_router" "router" {
  name = "router"
  external_network_id = data.vkcs_networking_network.extnet.id
}

# Подключение подсети subnet к маршрутизатору router
resource "vkcs_networking_router_interface" "router_interface_lb" {
  router_id = vkcs_networking_router.router.id
  subnet_id = vkcs_networking_subnet.subnet.id
}

# Создание порта db-port в сети manage-int-net
resource "vkcs_networking_port" "db_port" {
  name           = "db-port"
  network_id     = vkcs_networking_network.network.id
  admin_state_up = true
}

# Создание публичного IP для балансировщика нагрузки
resource "vkcs_networking_floatingip" "lb_fip" {
  pool    = data.vkcs_networking_network.extnet.name  
  port_id = vkcs_lb_loadbalancer.loadbalancer.vip_port_id
}

# Создание публичного IP для мониторинга
resource "vkcs_networking_floatingip" "monitoring_fip" {
  pool    = data.vkcs_networking_network.extnet.name
}

# Связывание публичного IP мониторинга с инстансом мониторинга
resource "vkcs_compute_floatingip_associate" "monitoring_fip" {
  floating_ip = vkcs_networking_floatingip.monitoring_fip.address
  instance_id = vkcs_compute_instance.monitoring.id
}

# Создание публичного IP для wp1
resource "vkcs_networking_floatingip" "wp1_fip" {
  pool    = data.vkcs_networking_network.extnet.name
}

# Связывание публичного IP wp1 с инстансом wp1
resource "vkcs_compute_floatingip_associate" "wp1_fip" {
  floating_ip = vkcs_networking_floatingip.wp1_fip.address
  instance_id = vkcs_compute_instance.wp_1.id
}

# Создание публичного IP для wp2
resource "vkcs_networking_floatingip" "wp2_fip" {
  pool    = data.vkcs_networking_network.extnet.name
}

# Связывание публичного IP wp2 с инстансом wp2
resource "vkcs_compute_floatingip_associate" "wp2_fip" {
  floating_ip = vkcs_networking_floatingip.wp2_fip.address
  instance_id = vkcs_compute_instance.wp_2.id
}

# Вывод публичного IP мониторинга
output "monitoring_instance_fip" {
  value = vkcs_networking_floatingip.monitoring_fip.address
}

# Вывод публичного IP балансировщика нагрузки
output "lb_instance_fip" {
  value = vkcs_networking_floatingip.lb_fip.address
}