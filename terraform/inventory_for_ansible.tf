# Создание файла инвентори для Ansible 
resource "local_file" "ansible_inventory" {
  filename = "inventory.yml"
  content = <<EOF
---
  all:
    children:
      wordpress:
        hosts:
          wordpress_01:
            ansible_host: ${vkcs_networking_floatingip.wp1_fip.address}
          wordpress_02:
            ansible_host: ${vkcs_networking_floatingip.wp2_fip.address}
      monitoring:
        hosts:
          monitoring_01:
            ansible_host: ${vkcs_networking_floatingip.monitoring_fip.address}
      wordpress_db:
        hosts:
          wordpress_db_01:
            ansible_host: ${data.vkcs_networking_port.db_loadbalancer_port.all_fixed_ips[0]}
    vars:
      wordpress_01_ip: ${vkcs_compute_instance.wp_1.network[0].fixed_ip_v4}
      wordpress_02_ip: ${vkcs_compute_instance.wp_2.network[0].fixed_ip_v4}
      monitoring_ip: ${vkcs_compute_instance.monitoring.network[0].fixed_ip_v4}
      wordpress_db_ip: ${data.vkcs_networking_port.db_loadbalancer_port.all_fixed_ips[0]}
      WORDPRESS_DB_USER: ${var.WORDPRESS_DB_USER}
      WORDPRESS_DB_PASSWORD: ${var.WORDPRESS_DB_PASSWORD}
      MOUNT_NFS: "mount -t ${local.mount_type} ${local.mount_device} /mnt${local.mount_dir}"
      MOUNT_DIR: /mnt${local.mount_dir}
      GRAF_PASS: ${var.GRAF_PASS}
      TEL_BOT: ${var.TEL_BOT}
      TEL_ID: ${var.TEL_ID}
EOF
}
