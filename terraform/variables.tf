# Переменные, используемые для настройки 
# и подключения к аккаунту VK Cloud

variable "VKCS_USERNAME" {
 type        = string
 description = "Электронная почта аккаунта VK Cloud"
 default     = "test@mail.com"
}

variable "VKCS_PASSWORD" {
 type        = string
 description = "Пароль от аккаунта VK Cloud"
 default     = "******"
}

variable "VKCS_PROJECT_ID" {
 type        = string
 description = "Идентификатор проекта в VK Cloud"
 default     = "*******"
}

variable "GRAF_PASS" {
 type        = string
 description = "Пароль от Grafana"
 default     = "*******"
}

variable "TEL_BOT" {
 type        = string
 description = "Телеграм бот"
 default     = "*******"
}

variable "TEL_ID" {
 type        = string
 description = "Идентификатор телеграм бота"
 default     = "*******"
}

variable "WORDPRESS_DB_USER" {
 type        = string
 description = "Имя пользователя базы данных Wordpress"
 default     = "wordpress"
}

variable "WORDPRESS_DB_PASSWORD" {
 type        = string
 description = "Пароль от базы данных Wordpress"
 default     = "Password1234567892!"
}

variable "wordpress_flavor" {
 type        = string
 description = "CPU и RAM для ВМ Wordpress"
 default     = "STD2-2-4"
}

variable "wordpress_image" {
 type        = string
 description = "Образ для ВМ Wordpress"
 default     = "Ubuntu-22.04-202208"
}

variable "monitoring_flavor" {
 type        = string
 description = "CPU и RAM для ВМ мониторинга"
 default     = "STD2-2-4"
}

variable "monitoring_image" {
 type        = string
 description = "Образ для ВМ мониторинга"
 default     = "Ubuntu-22.04-202208"
}

variable "key_pair_name" {
 type        = string
 description = "Имя ключевой пары для всех ВМ"
 default     = "devfixteam"
}

variable "availability_zone_name" {
 type        = string
 description = "Имя зоны доступности"
 default     = "MS1"
}

variable "db_instance_flavor" {
 type        = string
 description = "CPU и RAM для ВМ кластера базы данных"
 default     = "STD2-2-8"
}