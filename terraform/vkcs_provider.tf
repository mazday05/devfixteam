# vkcs провайдер позволяет управлять ресурсами в облаке Mail.Ru
terraform {
    required_providers {
        vkcs = {
            source = "vk-cs/vkcs"
            version = "~> 0.6.0" 
        }
    }
}

provider "vkcs" {
    # Укажите здесь свой username, password и project_id 
    # для доступа к облаку Mail.Ru
    username = var.VKCS_USERNAME 
    password = var.VKCS_PASSWORD
    project_id = var.VKCS_PROJECT_ID
    # Имя региона 
    region = "RegionOne"
    # URL для авторизации
    auth_url = "https://infra.mail.ru:35357/v3/" 
}
