# DevFixTeam
Состав команды:

1. @mazday05 (Михаил Н. Фирсов) Ansible
2. @razielset (Дмитрий М. Шпагин) Terraform
3. @anvkosykh (Антон В. Косых) Deploy - CI/CD
4. @Khodarin86 (Михаил Г. Ходарин) Monitoring (sponsor)
5. @vladmcsantax (Владислав О. Силаев) Telegram Alerting

Для того чтобы развернуть струкутуру через тераформ нужно следовать инструкции по запуску https://cloud.vk.com/docs/manage/tools-for-using-services/terraform/quick-start#937-tabpanel-1

Далее ввести запрашиваемые данные своего облака 

vkcs_username
vkcs_password
vkcs_project_id

и ожидать когда развернётся структура.

Infrastructure

![image info](./project_v2.png)


- Описание создаваемой инфраструктуры:
  - Баллансировщик нагрузки работающий с применением round-robin DNS
  - два сервера приложений WP
  - отказоустойчивый кластер БД MySQL 
  - создание резервной копии БД 1 раз в 12 часов глубиной хранения три дня
  - консистентность данных на серверах приложений достигается за счет использования отказоустойчивой NFS-share.
  - сервер мониторинга

- Для доступа к Web-интерфейсу Grafana использовать FIP инстанса сервера мониторинга, порт 3000. Логин: admin
- Загрузить в Настройки/CI-CD Settings/Secure Files .PEM-файл администратора проекта (сгенерированный индивидуально).
- Хранение артефактов джоб настроено на 1 час.
- Для сохранения возможности удаления инфраструктуры через terraform необходимо скачать артефакт.
- В проекте используются Gitlab Shared runner
- 


Используемые переменные:

GRAF_PASS (Masked)              - пароль для администратора Grafana
TEL_BOT (Masked)                - Telegram bot token 
TEL_ID (Masked)                 - ID чата назначения
VKCS_PASSWORD (Masked)          - пароль проекта VK Cloud
VKCS_PROJECT_ID                 - ID проекта VK Cloud
VKCS_USERNAME                   - e-mail администратора проекта VK Cloud
WORDPRESS_DB_PASSWORD (Masked)  - Пароль БД WP
WORDPRESS_DB_USER               - имя пользователя БД WP

